# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the  package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-20 09:02+0000\n"
"PO-Revision-Date: 2020-01-19 19:39+0000\n"
"Last-Translator: Moo <moose@mail.ru>\n"
"Language-Team: Lithuanian <https://translate.ubports.com/projects/ubports/"
"terminal-app/lt/>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n % 10 == 1 && (n % 100 < 11 || n % 100 > "
"19)) ? 0 : ((n % 10 >= 2 && n % 10 <= 9 && (n % 100 < 11 || n % 100 > 19)) ? "
"1 : 2);\n"
"X-Generator: Weblate 3.8\n"

#: ../src/app/qml/AlternateActionPopover.qml:68
msgid "Select"
msgstr "Pasirinkti"

#: ../src/app/qml/AlternateActionPopover.qml:73
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:176
msgid "Copy"
msgstr "Kopijuoti"

#: ../src/app/qml/AlternateActionPopover.qml:79
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:181
msgid "Paste"
msgstr "Įdėti"

#: ../src/app/qml/AlternateActionPopover.qml:86
msgid "Split horizontally"
msgstr "Padalyti horizontaliai"

#: ../src/app/qml/AlternateActionPopover.qml:92
msgid "Split vertically"
msgstr "Padalyti vertikaliai"

#: ../src/app/qml/AlternateActionPopover.qml:99
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:151
#: ../src/app/qml/TabsPage.qml:32
msgid "New tab"
msgstr "Nauja kortelė"

#: ../src/app/qml/AlternateActionPopover.qml:104
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:146
msgid "New window"
msgstr "Naujas langas"

#: ../src/app/qml/AlternateActionPopover.qml:109
msgid "Close App"
msgstr "Užverti programėlę"

#: ../src/app/qml/AuthenticationDialog.qml:25
msgid "Authentication required"
msgstr "Reikalingas tapatybės nustatymas"

#: ../src/app/qml/AuthenticationDialog.qml:27
msgid "Enter passcode or passphrase:"
msgstr ""

#: ../src/app/qml/AuthenticationDialog.qml:48
msgid "passcode or passphrase"
msgstr ""

#: ../src/app/qml/AuthenticationDialog.qml:58
msgid "Authenticate"
msgstr "Nustatyti tapatybę"

#: ../src/app/qml/AuthenticationDialog.qml:70
#: ../src/app/qml/ConfirmationDialog.qml:42
msgid "Cancel"
msgstr "Atsisakyti"

#: ../src/app/qml/AuthenticationService.qml:64
msgid "Authentication failed"
msgstr "Nepavyko nustatyti tapatybę"

#: ../src/app/qml/AuthenticationService.qml:83
msgid "No SSH server running."
msgstr "Nėra veikiančių SSH serverių."

#: ../src/app/qml/AuthenticationService.qml:84
msgid "SSH server not found. Do you want to proceed in confined mode?"
msgstr ""

#: ../src/app/qml/ConfirmationDialog.qml:32
msgid "Continue"
msgstr "Tęsti"

#: ../src/app/qml/KeyboardBar.qml:197
msgid "Change Keyboard"
msgstr "Keisti klaviatūrą"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:35
msgid "Control Keys"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:38
msgid "Function Keys"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:41
msgid "Scroll Keys"
msgstr "Slinkimo klavišai"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:44
msgid "Command Keys"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:52
msgid "Ctrl"
msgstr "Vald"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:55
msgid "Fn"
msgstr "Fn"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:58
msgid "Scr"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:61
msgid "Cmd"
msgstr ""

#. TRANSLATORS: This is the name of the Control key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:78
msgid "CTRL"
msgstr "VALD"

#. TRANSLATORS: This is the name of the Alt key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:81
msgid "Alt"
msgstr "Alt"

#. TRANSLATORS: This is the name of the Shift key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:84
msgid "Shift"
msgstr "Lyg2"

#. TRANSLATORS: This is the name of the Escape key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:97
msgid "Esc"
msgstr ""

#. TRANSLATORS: This is the name of the Page Up key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:100
msgid "PgUp"
msgstr ""

#. TRANSLATORS: This is the name of the Page Down key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:103
msgid "PgDn"
msgstr ""

#. TRANSLATORS: This is the name of the Delete key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:106
msgid "Del"
msgstr ""

#. TRANSLATORS: This is the name of the Home key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:109
msgid "Home"
msgstr "Prad"

#. TRANSLATORS: This is the name of the End key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:112
msgid "End"
msgstr "Pab"

#. TRANSLATORS: This is the name of the Tab key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:115
msgid "Tab"
msgstr "Tab"

#. TRANSLATORS: This is the name of the Enter key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:118
msgid "Enter"
msgstr ""

#: ../src/app/qml/NotifyDialog.qml:25
msgid "OK"
msgstr "Gerai"

#: ../src/app/qml/Settings/BackgroundOpacityEditor.qml:26
msgid "Background opacity:"
msgstr "Fono nepermatomumas:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:79
msgid "R:"
msgstr "R:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:87
msgid "G:"
msgstr "G:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:95
msgid "B:"
msgstr "B:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:99
msgid "Undo"
msgstr "Atšaukti"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:55
msgid "Text"
msgstr "Tekstas"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:66
msgid "Font:"
msgstr "Šriftas:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:91
msgid "Font Size:"
msgstr "Šrifto dydis:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:119
msgid "Colors"
msgstr "Spalvos"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:124
msgid "Ubuntu"
msgstr "Ubuntu"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:125
msgid "Green on black"
msgstr "Žalia ant juodos"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:126
msgid "White on black"
msgstr "Balta ant juodos"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:127
msgid "Black on white"
msgstr "Juoda ant baltos"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:128
msgid "Black on random light"
msgstr "Juoda ant atsitiktinės šviesios"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:129
msgid "Linux"
msgstr "Linux"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:130
msgid "Cool retro term"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:131
msgid "Dark pastels"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:132
msgid "Black on light yellow"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:133
msgid "Customized"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:144
msgid "Background:"
msgstr "Fonas:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:152
msgid "Text:"
msgstr "Tekstas:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:164
msgid "Normal palette:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:171
msgid "Bright palette:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:180
msgid "Preset:"
msgstr "Išankstinė nuostata:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:197
msgid "Layouts"
msgstr "Išdėstymai"

#: ../src/app/qml/Settings/SettingsPage.qml:33
#, fuzzy
msgid "close"
msgstr "Užverti"

#: ../src/app/qml/Settings/SettingsPage.qml:39
msgid "Preferences"
msgstr "Nuostatos"

#: ../src/app/qml/Settings/SettingsPage.qml:50
msgid "Interface"
msgstr "Sąsaja"

#: ../src/app/qml/Settings/SettingsPage.qml:54
msgid "Shortcuts"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:60
#, qt-format
msgid "Showing %1 of %2"
msgstr "Rodoma %1 iš %2"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:145
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:150
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:155
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:160
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:165
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:170
msgid "File"
msgstr "Failas"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:156
msgid "Close terminal"
msgstr "Užverti terminalą"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:161
msgid "Close all terminals"
msgstr "Užverti visus terminalus"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:166
msgid "Previous tab"
msgstr "Ankstesnė kortelė"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:171
msgid "Next tab"
msgstr "Kita kortelė"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:175
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:180
msgid "Edit"
msgstr "Taisyti"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:185
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:190
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:195
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:200
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:205
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:210
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:215
msgid "View"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:186
msgid "Toggle fullscreen"
msgstr "Perjungti visą ekraną"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:191
msgid "Split terminal horizontally"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:196
msgid "Split terminal vertically"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:201
msgid "Navigate to terminal above"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:206
msgid "Navigate to terminal below"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:211
msgid "Navigate to terminal on the left"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:216
msgid "Navigate to terminal on the right"
msgstr ""

#: ../src/app/qml/Settings/SettingsWindow.qml:26
msgid "Terminal Preferences"
msgstr "Terminalo nuostatos"

#: ../src/app/qml/Settings/ShortcutRow.qml:78
msgid "Enter shortcut…"
msgstr ""

#: ../src/app/qml/Settings/ShortcutRow.qml:80
msgid "Disabled"
msgstr "Išjungta"

#: ../src/app/qml/TabsPage.qml:26
msgid "Tabs"
msgstr "Kortelės"

#: ../src/app/qml/TerminalPage.qml:63 ubuntu-terminal-app.desktop.in.in.h:1
msgid "Terminal"
msgstr "Terminalas"

#: ../src/app/qml/TerminalPage.qml:302
msgid "Selection Mode"
msgstr ""

#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:291
#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:320
msgid "Un-named Color Scheme"
msgstr ""

#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:468
msgid "Accessible Color Scheme"
msgstr ""

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:533
msgid "Open Link"
msgstr "Atverti nuorodą"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:534
msgid "Copy Link Address"
msgstr "Kopijuoti nuorodos adresas"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:538
msgid "Send Email To…"
msgstr "Siųsti el. paštą į…"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:539
msgid "Copy Email Address"
msgstr "Kopijuoti el. pašto adresą"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:39
msgid "Match case"
msgstr "Skirti didžiąsias ir mažąsias raides"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:45
msgid "Regular expression"
msgstr "Reguliarusis reiškinys"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:49
msgid "Higlight all matches"
msgstr "Paryškinti visus atitikmenis"

#: ../src/plugin/qmltermwidget/lib/Vt102Emulation.cpp:982
msgid ""
"No keyboard translator available.  The information needed to convert key "
"presses into characters to send to the terminal is missing."
msgstr ""

#: ../src/plugin/qmltermwidget/lib/qtermwidget.cpp:406
msgid "Color Scheme Error"
msgstr ""

#: ../src/plugin/qmltermwidget/lib/qtermwidget.cpp:407
#, qt-format
msgid "Cannot load color scheme: %1"
msgstr ""
